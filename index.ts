/**
 * Created by gerd on 18.10.15.
 */
import Hapi = require('hapi');

import opn = require('opn');

import api = require('./lib/api');

// Create a server with a host and port
var server = new Hapi.Server();

server.register(require('inert'), error => {
    "use strict";

    if (error) {
        throw error;
    }

    server.connection({
        host: '127.0.0.1',
        port: 8000
    });

// Add the route
    server.route({
        method: 'GET',
        path: '/sections/{param*}',
        handler: {
            directory: {
                path: 'sections'
            }
        }
    });
    server.route({
        method: 'GET',
        path: '/app/{param*}',
        handler: {
            directory: {
                path: 'app'
            }
        }
    });

    // REST API
    server.route({
        method: 'GET',
        path: '/api/burgers',
        handler: (req, reply) => {
            api.getBurgers()
                .then(burgers => reply(burgers));
        }
    });
    server.route({
        method: 'GET',
        path: '/api/burgers/{name}',
        handler: (req, reply) => {
            api.getBurgerByName(req.params['name'])
                .then(burger => reply(burger));
        }
    });
    server.route({
        method: 'PUT',
        path: '/api/burgers/{name}',
        handler: (req, reply) => {
            api.updateBurger(req.params['name'], req.payload)
                .then(burger => reply(burger));
        }
    });

// Start the server
    server.start(function () {
        console.log('Server running at:', server.info.uri);

        opn(`${server.info.uri}/sections`);
    });

});