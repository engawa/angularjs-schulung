/**
 * Created by gerd on 18.10.15.
 */
/// <reference path="../../typings/tsd.d.ts" />
var db = require('../db');
/**
 *
 * @returns {Promise<Burger[]>}
 */
function getBurgers() {
    return db.getBurgers();
}
exports.getBurgers = getBurgers;
/**
 *
 * @param name
 * @returns {Promise<R>|Promise<T>}
 */
function getBurgerByName(name) {
    return db.getBurgerByName(name);
}
exports.getBurgerByName = getBurgerByName;
/**
 *
 * @param {string} name
 * @param {Burger} burger
 * @returns {Promise<R>|Promise<T>}
 */
function updateBurger(name, burger) {
    return db.updateBurger(name, burger);
}
exports.updateBurger = updateBurger;
//# sourceMappingURL=index.js.map