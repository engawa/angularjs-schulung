/**
 * Created by gerd on 18.10.15.
 */
/// <reference path="../../typings/tsd.d.ts" />

import Burger from '../model/burger';

import db = require('../db');

/**
 *
 * @returns {Promise<Burger[]>}
 */
export function getBurgers():Promise<Burger[]> {
    return db.getBurgers();
}

/**
 *
 * @param name
 * @returns {Promise<R>|Promise<T>}
 */
export function getBurgerByName(name:string):Promise<Burger> {
    return db.getBurgerByName(name);
}

/**
 *
 * @param {string} name
 * @param {Burger} burger
 * @returns {Promise<R>|Promise<T>}
 */
export function updateBurger(name:string, burger:Burger):Promise<Burger> {
    return db.updateBurger(name, burger);
}