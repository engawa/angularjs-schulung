/**
 * Created by gerd on 31.10.15.
 */

export default class Burger {
    name: string;
    description: string;
    type: string;
    price: number;

    constructor(name: string, description: string, type: string, price: number = 0) {
        this.name = name;
        this.description = description;
        this.type = type;
        this.price = price;
    }

    static burgers: Array<Burger> = [
        new Burger('Luftsprung', 'Walnussbratling mit Gorgonzolacrème und Rauke', 'V', 7.8),
        new Burger('Landei', 'gegrillte Hähnchenbrust, viele Sprossen und Schnittlauchsoße', 'B', 7.5),
        new Burger('Klassik', 'Die Mutter aller Burger', 'C', 6.5),
        new Burger('Heumilchkäse', 'der Klassiker mit Käse', 'C', 6.9),
        new Burger('Käse & Speck', 'Heumilchkäse & Speck', 'C', 7.8),
        new Burger('Elsässer', 'Brie & Preiselbeeren', 'C', 7.8),
        new Burger('Hans im Glück', 'mit Parmaschinken, italienischem Hartkäse, Rauke & Balsamico', 'C', 8.2),
        new Burger('Geissbock', 'Ziegenkäse & Feigensoße', 'C', 7.8),
        new Burger('Caesar', 'mit Speck & Parmesandressing', 'C', 7.8),
        new Burger('Avocado', 'Heumilchkäse & Avocadocreme', 'C', 7.8),
        new Burger('Birkenwald', 'Champignons & Kräuterfrischkäse', 'C', 7.8),
        new Burger('Stiefel', 'Mozzarella, viel Tomate & Rauke', 'C', 7.8),
        new Burger('Gorgonzola', 'mit würziger Gorgonzolacreme', 'C', 7.8),
        new Burger('Wilder Westen', 'Heumilchkäse, Grillsoße & Röstzwiebeln', 'C', 7.8),
        new Burger('Glücksschmied', 'Heumilchkäse & rote Pfeffersoße', 'C', 7.8),
        new Burger('Heimweh', 'Gorgonzola & sonnengetrocknete Tomaten', 'C', 7.8),
        new Burger('Eitler Gockel', 'gegrillte Hähnchenbrust, Wasabi, Sprossen & süße Chilisoße', 'C', 7.8),
        new Burger('Gaumenfreude', 'Hähnchenbrust mit Mozzarella, sonnengetrockneten Tomaten & Rauke', 'C', 7.8),
        new Burger('Stallbursche', 'gegrillte Hähnchenbrust, Speck & Heumilchkäse', 'C', 7.8),
        new Burger('Hoffnungsträger', 'saftiges Rindfleisch mit Rauke, italienischem Hartkäse & Parmesandressing', 'B', 7.5),
        new Burger('Wald & Wiese', 'pikanter Gemüsebratling mit Heumilchkäse & Avocadocreme', 'B', 7.5),
        new Burger('Abendrot', 'Oliven-Tomatenbratling mit Grillgemüse, Rauke & italienischem Hartkäse', 'V', 7.8),
        new Burger('Weisse Knolle', 'pikanter Gemüsebratling mit Knoblauchmajo', 'V', 7.8),
        new Burger('Heuernte', 'Walnussbratling mit Gorgonzolacreme & sonnengetrockneten Tomaten', 'V', 7.8),
        new Burger('Seelenheil', 'Oliven-Tomatenbratling mit Mozzarella & Rauke', 'V', 7.8),
        new Burger('Wolpertinger', 'pikanter Gemüsebratling mit Heumilchkäse & Avocadocreme', 'V', 7.8),
        new Burger('Dreikäsehoch', 'Oliven-Tomatenbratling mit Heumilchkäse, Gorgonzolacreme & italienischem Hartkäse', 'V', 7.8),
        new Burger('Veganer Burger', 'würziger Weizen-Bratling mit extra Tomaten, Rauke & Sprossen. Selbstverständlich ist auch die Soße 100% vegan', 'V', 7.8),
        new Burger('Wurzelsepp', 'Walnussbratling mit Heumilchkäse, Nüssen & Sprossen', 'V', 7.8),
        new Burger('Feuerring', 'pikanter Gemüsebratling mit Heumilchkäse & roter Pfeffersoße', 'V', 7.8)
    ];
}