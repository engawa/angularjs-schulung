/**
 * Created by gerd on 31.10.15.
 */
/// <reference path="../../typings/tsd.d.ts" />
var burger_1 = require("../model/burger");
var ForerunnerDB = require('forerunnerdb');
var fdb = new ForerunnerDB();
var db = fdb.db('cleanburger');
var burgers = db.collection('burgers', {
    primaryKey: 'name'
});
/**
 *
 * @type {Burger[]}
 */
burgers.setData(burger_1.default.burgers);
/**
 *
 * @returns {Promise<Burger[]>}
 */
function getBurgers() {
    return Promise.resolve(burgers.find());
}
exports.getBurgers = getBurgers;
/**
 *
 * @param {string} name
 * @returns {Promise<Burger>}
 */
function getBurgerByName(name) {
    return Promise.resolve(burgers.findOne({
        name: name
    }));
}
exports.getBurgerByName = getBurgerByName;
/**
 *
 * @param name
 * @param burger
 * @returns {Promise<T>|Promise<R>}
 */
function updateBurger(name, burger) {
    return Promise.resolve(burgers.updateById(name, burger));
}
exports.updateBurger = updateBurger;
//# sourceMappingURL=index.js.map