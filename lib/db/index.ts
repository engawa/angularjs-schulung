/**
 * Created by gerd on 31.10.15.
 */
/// <reference path="../../typings/tsd.d.ts" />

import Burger from "../model/burger";

const ForerunnerDB = require('forerunnerdb');

const fdb = new ForerunnerDB();

const db = fdb.db('cleanburger');

const burgers = db.collection('burgers', {
    primaryKey: 'name'
});

/**
 *
 * @type {Burger[]}
 */
burgers.setData(Burger.burgers);

/**
 *
 * @returns {Promise<Burger[]>}
 */
export function getBurgers():Promise<Burger[]> {
    return Promise.resolve(burgers.find());
}

/**
 *
 * @param {string} name
 * @returns {Promise<Burger>}
 */
export function getBurgerByName(name:string):Promise<Burger> {
    return Promise.resolve(burgers.findOne({
        name
    }));
}

/**
 *
 * @param name
 * @param burger
 * @returns {Promise<T>|Promise<R>}
 */
export function updateBurger(name:string, burger:Burger):Promise<Burger> {
    return Promise.resolve(burgers.updateById(name, burger));
}