/**
 * Created by gerd on 02.12.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.components.burgerpreview', [])
        .component('cbBurgerPreview', {
            templateUrl: 'src/components/burgerpreview/burgerpreview.html',
            controller: 'BurgerPreviewCtrl as preview',
            bindings: {
                burger: '=',
                maxchars: '@',
                ratable: '=isRatable',
                onRate: '&'
            }
        })
        .controller('BurgerPreviewCtrl', BurgerPreviewCtrl);

    /**
     *
     * @constructor
     */
    function BurgerPreviewCtrl($log) {
        // Public API
        this.isVegetarian = isVegetarian;
        this.rateBurger = rateBurger;

        /**
         *
         * @param {Object} burger
         * @returns {boolean} true, if the given burger is suited for vegetarians, otherwise false
         */
        function isVegetarian(burger) {
            return burger.type === 'V';
        }

        /**
         * Set a rating for the burger
         * @param {number} value
         */
        function rateBurger(value) {
            $log.debug(value);
            this.onRate({
                rating: value // call the passed in function reference with a map of arguments
            });
        }
    }


})();