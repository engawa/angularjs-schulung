/**
 * Created by gerd on 25.10.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.views.details', [])
        .controller('DetailsCtrl', DetailsCtrl);

    DetailsCtrl.$inject = ['BurgersService', '$routeParams', '$log'];

    /* @ngInject */
    function DetailsCtrl(BurgersService, $routeParams, $log) {
        var vm = this;

        // Public API
        this.updateRating = updateRating;

        BurgersService.getBurgerByName($routeParams.name)
            .then(function (burger) {
                vm.burger = burger;
            });

        /**
         *
         * @param {Burger} burger
         * @param {number} rating
         */
        function updateRating(burger, rating) {
            burger.rating = rating;
            BurgersService.updateBurger(burger)
                .then(function () {
                    $log.debug(rating);
                });
        }
    }

})();

