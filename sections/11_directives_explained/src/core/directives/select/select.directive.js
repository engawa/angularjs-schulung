/**
 * Created by gerd on 25.10.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.core.directives.select', [])
        .directive('cbSelect', SelectDirective);

    /* @ngInject */
    function SelectDirective() {
        var directive = {
            link: link,
            restrict: 'A' // use as an attribute, no need for a new scope
        };
        return directive;

        function link(scope, element, attrs) {
            var event;

            event = attrs['cbSelect'] || 'focus';

            element.on(event, function (ev) {
                if (angular.isFunction(ev.target.select)) {
                    ev.target.select();
                }
            });
        }
    }

})();

