/**
 * Created by gerd on 17.10.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.core.services.data.burgers', [])
        .service('BurgersService', BurgersService);

    /**
     *
     * @constructor
     */
    function BurgersService($http, $log, REST_URL) { // services can be injected to build a tree of dependencies
        this.$http = $http;
        this.$log = $log;
        this.REST_URL = REST_URL || '/api/burgers';

        // local caching
        this.burgers = [];
    }

    /**
     *
     * @returns {*[]}
     */
    BurgersService.prototype.getBurgers = function () {
        return this.$http.get(this.REST_URL)
            .then(function(response) {
                this.$log.debug(response); // log response object to get some insight
                return (this.burgers = response.data);
            }.bind(this)); // bind the callback function to the lexical ```this```
    };


    /**
     *
     * @param name
     * @returns {*}
     */
    BurgersService.prototype.getBurgerByName = function (name) {
        return this.$http.get(this.REST_URL + '/' + name)
            .then(function(response) {
                this.$log.debug(response); // log response object to get some insight
                return response.data;
            }.bind(this)); // bind the callback function to the lexical ```this```
    };

    /**
     *
     * @param burger
     * @returns {*}
     */
    BurgersService.prototype.updateBurger = function (burger) {
        return this.$http.put(this.REST_URL + '/' + burger.name, burger) // pass the burger as payload
            .then(function(response) {
                this.$log.debug(response); // log response object to get some insight
                return response.data;
            }.bind(this)); // bind the callback function to the lexical ```this```
    };

    /**
     *
     * @returns {string[]}
     */
    BurgersService.prototype.getTypes = function() {
        return angular.copy(['V', 'C', 'B']);
    };

})();

