/**
 * Created by gerd on 17.10.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.core.filters', [
            'cleanburger.core.filters.burgertype',
            'cleanburger.core.filters.truncate'
        ]);
})();