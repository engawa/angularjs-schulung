/**
 * Created by gerd on 18.10.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.core.filters.burgertype', [])
        .filter('cbBurgertype', cbBurgertype);

    /**
     *
     * @returns {cbBurgertypeFilter}
     */
    function cbBurgertype(/* DI */) { // dependency injection is working here
        return cbBurgertypeFilter;

        ////////////////

        /**
         * The actual filter function
         * @param type
         * @returns {*}
         */
        function cbBurgertypeFilter(type) {
            switch (type) {
                case 'C':
                    return 'Classic';
                case 'B':
                    return 'Breadless';
                case 'V':
                    return 'Vegetarian'
            }
            return type;
        }
    }

})();

