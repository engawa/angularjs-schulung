/**
 * Created by gerd on 25.10.15.
 */
describe('Controller: DetailsCtrl', function () {
    "use strict";

    var vm;

    beforeEach(module('cleanburger'));

    // provide a stub of the service
    beforeEach(module(function ($provide) {
        $provide.service('BurgersService', function BurgersService() {
            this.getBurgerByName = function (name) {
                return {
                    then: angular.noop
                };
            };
            this.updateBurger = function (burger) {
                return {
                    then: angular.noop
                };
            };
        });
    }));

    beforeEach(inject(function ($controller, $rootScope) {
        // 'controller as' syntax demands injection of the '$scope'
        vm = $controller('DetailsCtrl as vm', {
            $scope: $rootScope.$new()
        });
    }));

    describe('Public API', function () {
        it('should expose a updateRating() method', function () {
            expect(vm.updateRating).toBeDefined();
            expect(vm.updateRating).toEqual(jasmine.any(Function));
        });
    });

    describe('updateRating()', function () {
        var BurgersService;

        beforeEach(inject(function(_BurgersService_) {
            BurgersService = _BurgersService_;
        }));

        it('should delegate to the BurgersService', function () {
            spyOn(BurgersService, 'updateBurger').and.callThrough();

            var burger, rating;

            burger = {
                name: 'foo'
            };

            rating = 5;

            vm.updateRating(burger, rating);

            expect(burger.rating).toEqual(5);
            expect(BurgersService.updateBurger).toHaveBeenCalled();
            expect(BurgersService.updateBurger).toHaveBeenCalledWith(burger);
        });

    });
});