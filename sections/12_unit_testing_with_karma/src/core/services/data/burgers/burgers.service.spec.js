/**
 * Created by gerd on 25.10.15.
 */
describe('Service: BurgersService', function () {
    "use strict";

    var BurgersService, $httpBackend;

    var REST_URL = '/api/burgers';

    beforeEach(module('cleanburger.core.services.data.burgers'));

    // stub of the REST_URL injected into the service
    beforeEach(module(function ($provide) {
        $provide.constant('REST_URL', REST_URL);
    }));

    // wrap the injectable's name into underscores to avoid assignment errors
    beforeEach(inject(function (_BurgersService_, _$httpBackend_) {
        BurgersService = _BurgersService_;
        $httpBackend = _$httpBackend_;
    }));

    describe('Public API', function () {
        it('should expose a getBurgers() method', function () {
            expect(BurgersService.getBurgers).toBeDefined();
            expect(BurgersService.getBurgers).toEqual(jasmine.any(Function));
        });
        it('should expose a getBurgerByName() method', function () {
            expect(BurgersService.getBurgerByName).toBeDefined();
            expect(BurgersService.getBurgerByName).toEqual(jasmine.any(Function));
        });
        it('should expose an updateBurger() method', function () {
            expect(BurgersService.updateBurger).toBeDefined();
            expect(BurgersService.updateBurger).toEqual(jasmine.any(Function));
        });
        it('should expose a getTypes() method', function () {
            expect(BurgersService.getTypes).toBeDefined();
            expect(BurgersService.getTypes).toEqual(jasmine.any(Function));
        });
    });

    describe('getBurgers()', function () {
        beforeEach(function () {
            $httpBackend.whenGET(REST_URL)
                .respond([{}]);
        });
        afterEach(function () {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });

        it('should return a Promise', function () {
            expect(BurgersService.getBurgers().then).toBeDefined();
            expect(BurgersService.getBurgers().then).toEqual(jasmine.any(Function));
            $httpBackend.flush();
        });
        it('should call the REST API', function () {
            $httpBackend.expectGET(REST_URL);
            BurgersService.getBurgers();
            $httpBackend.flush();
        });
        it('should be resolved with a list of burgers', function () {
            BurgersService.getBurgers()
                .then(function (burgers) {
                    expect(burgers).toEqual(jasmine.any(Array));
                });
            $httpBackend.flush();
        });
    });

    describe('getTypes()', function () {
        it('should return an array of types', function () {
            expect(BurgersService.getTypes()).toEqual(jasmine.any(Array));
            expect(BurgersService.getTypes().length).toBe(3);
        });
        it('should always return a new array', function () {
            expect(BurgersService.getTypes()).not.toBe(BurgersService.getTypes());
        });
    });


});