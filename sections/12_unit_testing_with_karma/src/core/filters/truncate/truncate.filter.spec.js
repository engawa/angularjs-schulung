/**
 * Created by gerd on 25.10.15.
 */
describe('Filter: cbTruncate', function () {
    "use strict";

    var cbTruncate;

    beforeEach(module('cleanburger.core.filters.truncate'));

    beforeEach(inject(function ($filter) {
        cbTruncate = $filter('cbTruncate');
    }));

    it('should truncate the given string to the given length', function () {
        expect(cbTruncate('123456789', 5)).toBe('12345');
    });

    it('should append the given suffix', function () {
        expect(cbTruncate('123456789', 5, '...')).toBe('12345...');
    });

});