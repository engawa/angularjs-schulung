/**
 * Created by gerd on 25.10.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.core.filters.truncate', [])
        .filter('cbTruncate', cbTruncate);

    function cbTruncate() {
        return cbTruncateFilter;

        ////////////////

        /**
         *
         * @param str
         * @param len
         * @param suffix
         */
        function cbTruncateFilter(str, len, suffix) {
            str = str || '';
            len = len || str.length;
            suffix = suffix || '';

            return str.substr(0, len) + (len < str.length ? suffix : '');
        }
    }

})();

