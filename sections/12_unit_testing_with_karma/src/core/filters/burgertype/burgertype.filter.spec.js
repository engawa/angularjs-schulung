/**
 * Created by gerd on 25.10.15.
 */
describe('Filter: cbBurgertype', function () {
    "use strict";

    var cbBurgertype;

    beforeEach(module('cleanburger.core.filters.burgertype'));

    beforeEach(inject(function ($filter) {
        cbBurgertype = $filter('cbBurgertype');
    }));

    it('should return "Classic" for type "C"', function () {
        expect(cbBurgertype('C')).toBe('Classic');
    });

    it('should return "Breadless" for type "B"', function () {
        expect(cbBurgertype('B')).toBe('Breadless');
    });

    it('should return "Vegetarian" for type "V"', function () {
        expect(cbBurgertype('V')).toBe('Vegetarian');
    });

    it('should return the given string for an unknown type', function() {
        expect(cbBurgertype('X')).toBe('X');
        expect(cbBurgertype('Y')).toBe('Y');
        expect(cbBurgertype('Z')).toBe('Z');
    });

});