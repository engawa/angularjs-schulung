/**
 * Created by gerd on 17.10.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.components.burgerpreview', [])
        .directive('cbBurgerPreview', BurgerPreviewComponent);
    /**
     *
     */
    function BurgerPreviewComponent($log) {
        var directive = {
            restrict: 'E',
            templateUrl: 'src/components/burgerpreview/burgerpreview.html',
            scope: {}, // create an isolated scope for each component instance
            controller: BurgerPreviewCtrl,
            controllerAs: 'preview',
            bindToController: {
                burger: '=', // add two-way binding for the data to be displayed
                maxchars: '@', // add attribute binding to configure component instance
                ratable: '=isRatable', // optionally add attribute name (applies to =, @, and &)
                onRate: '&' // add function reference to be called when component changes
            },
            transclude: true, // allows for passing in HTML from outside
            require: ['cbBurgerPreview'], // enables access to sibling / parent controller(s)
            link: function (scope, element, attrs, ctrls) { // the link function is called once the component has been instantiated

                // in some cases one might need to actively watch model changes
                scope.$watch('::preview.burger', function (burger) {
                    if (burger) {
                        scope.preview.rating = burger.rating;

                        // it is possible to call methods of the directive's controller
                        ctrls[0].rateBurger(5);
                    }
                });

                // it is possible to use the scope to emit and receive events
                scope.$on('cb.something', function (ev, message) {
                    $log.debug(ev, message);
                });

                // it is possible to do clean up before the scope is destroyed
                scope.$on('$destroy', function(ev) {
                    $log.debug(ev);
                });

                // it is possible to register event listeners on the DOM node (via jQuery)
                element.on('mouseleave', function (ev) {
                    $log.debug(ev);

                    scope.$emit('cb.something', 'happened');
                });

                // it is possible to manipulate the node attributes
                attrs.$set('maxchars', 45);
            }
        };
        return directive;
    }

    /**
     *
     * @constructor
     */
    function BurgerPreviewCtrl($log) {
        // Public API
        this.isVegetarian = isVegetarian;
        this.rateBurger = rateBurger;

        /**
         *
         * @param {Object} burger
         * @returns {boolean} true, if the given burger is suited for vegetarians, otherwise false
         */
        function isVegetarian(burger) {
            return burger.type === 'V';
        }

        /**
         * Set a rating for the burger
         * @param {number} value
         */
        function rateBurger(value) {
            $log.debug(value);
            this.onRate({
                rating: value // call the passed in function reference with a map of arguments
            });
        }
    }

})();

