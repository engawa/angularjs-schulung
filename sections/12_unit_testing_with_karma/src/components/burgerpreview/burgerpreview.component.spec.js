/**
 * Created by gerd on 25.10.15.
 */
describe('Component: cbBurgerPreview', function () {
    "use strict";

    var $compile, $rootScope;

    beforeEach(module('cleanburger'));

    beforeEach(inject(function (_$compile_, _$rootScope_) {
        $compile = _$compile_;
        $rootScope = _$rootScope_;
    }));

    it('should be instantiated as element', function () {
        var template, element;

        template = '<cb-burger-preview></cb-burger-preview>';

        element = $compile(template)($rootScope);
        $rootScope.$apply();

        expect(element.html()).toBeTruthy();
    });

    it('should transclude a headline', function() {
        var template, element;

        template = '<cb-burger-preview><h1>foo</h1></cb-burger-preview>';

        element = $compile(template)($rootScope);
        $rootScope.$apply();

        expect(element.find('h1').text()).toBe('foo');
    });

    it('should pass in a maxchars value', function() {
        var template, element, scope;

        template = '<cb-burger-preview maxchars="25"></cb-burger-preview>';

        element = $compile(template)($rootScope);
        $rootScope.$apply();
        scope = element.children().scope();

        expect(scope.preview).toBeDefined();
        expect(scope.preview.maxchars).toEqual('25');
    });

});