/**
 * Created by gerd on 17.10.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.components.burgerlist', [])
        .directive('cbBurgerList', BurgerListComponent); // use camel case to declare the name of the directive

    function BurgerListComponent() {
        // Directive Definition Object (DDO)
        var directive = {
            restrict: 'E', // create an instance by using an HTML element, i.e. <cb-burger-list>
            templateUrl: 'src/components/burgerlist/burgerlist.html',
            controller: BurgerListCtrl,
            controllerAs: 'list'
        };
        return directive;
    }

    /**
     *
     * @constructor
     */
    function BurgerListCtrl(BurgersService) {
        // Public API
        this.getBurgerCount = getBurgerCount;

        // declare a list of burgers to be displayed in the view
        this.burgers = BurgersService.getBurgers();

        /**
         *
         * @returns {Number}
         */
        function getBurgerCount() {
            return this.burgers.length;
        }
    }

})();