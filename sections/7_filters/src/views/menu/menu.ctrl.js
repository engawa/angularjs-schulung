/**
 * Created by gerd on 17.10.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.views.menu', []) // declare a new module by giving it a name
        .controller('MenuCtrl', MenuCtrl);

    /**
     *
     * @constructor
     */
    function MenuCtrl() {
        // Public API
        this.getBurgerCount = getBurgerCount;

        // declare a list of burgers to be displayed in the view
        this.burgers = [
            {
                "name": "Luftsprung",
                "description": "Walnussbratling mit Gorgonzolacrème und Rauke",
                "type": "V",
                "price": 7.8
            },
            {
                "name": "Landei",
                "description": "gegrillte Hähnchenbrust, viele Sprossen und Schnittlauchsoße",
                "type": "B",
                "price": 7.5
            },
            {
                "name": "Klassik",
                "description": "Die Mutter aller Burger",
                "type": "C",
                "price": 6.5
            },
            {
                "name": "Heumilchkäse",
                "description": "der Klassiker mit Käse",
                "type": "C",
                "price": 6.9
            },
            {
                "name": "Käse & Speck",
                "description": "Heumilchkäse & Speck",
                "type": "C",
                "price": 7.8
            },
            {
                "name": "Elsässer",
                "description": "Brie & Preiselbeeren",
                "type": "C",
                "price": 7.8
            }
        ];

        // calculate length of longest description
        this.maxchars = this.burgers.reduce(function(length, burger) {
            return burger.description.length > length ? burger.description.length : length;
        }, 0);

        /**
         *
         * @returns {Number}
         */
        function getBurgerCount() {
            return this.burgers.length;
        }
    }

})();

