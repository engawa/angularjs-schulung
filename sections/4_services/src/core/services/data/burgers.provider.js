/**
 * Created by gerd on 27.10.15.
 */
(function () {
    'use strict';

    var _url; // declare a variable which is accessible inside provider and factory / service

    angular
        .module('cleanburger.core.services.data')
        .provider('BurgersData', function () {
            // declare method(s) which can be called in the configuration phase
            this.setBaseUrl = function (url) {
                _url = url;
            };
            // declare a $get field which points to the actual service implementation
            this.$get = BurgersData;
        });

    /* @ngInject */
    function BurgersData($http) {
        var service = {
            getBurgers: getBurgers
        };
        return service;

        ////////////////

        /**
         *
         * @returns {*}
         */
        function getBurgers() {
            return $http.get(_url) // use variable(s) set in provider method(s)
                .then(function (response) {
                    return response.data;
                });
        }
    }

})();

