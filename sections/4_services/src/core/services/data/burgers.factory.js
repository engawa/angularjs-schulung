/**
 * Created by gerd on 17.10.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.core.services.data')
        .factory('BurgersFactory', BurgersFactory); // use 'factory' to declare a function that returns a service instance

    /**
     *
     * @returns {{getBurgers: getBurgers}}
     */
    function BurgersFactory() {
        // Public API
        var service = { // the object returned from the factory function gets instantiated
            getBurgers: getBurgers
        };
        return service;

        ////////////////

        /**
         *
         * @returns {*[]}
         */
        function getBurgers() {
            return [{
                name: 'Luftsprung',
                description: 'Walnussbratling mit Gorgonzolacrème und Rauke',
                type: 'V',
                price: 7.8
            }, {
                name: 'Landei',
                description: 'gegrillte Hähnchenbrust, viele Sprossen und Schnittlauchsoße',
                type: 'B',
                price: 7.5
            }, {
                name: 'Klassik',
                description: 'Die Mutter aller Burger',
                type: 'C',
                price: 6.5
            }];
        }
    }

})();

