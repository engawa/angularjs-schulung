/**
 * Created by gerd on 17.10.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.core.services.data')
        .service('BurgersService', BurgersService); // use 'service' to declare a constructor function that gets instantiated with 'new'

    /**
     *
     * @constructor
     */
    function BurgersService() {
    }

    /**
     *
     * @returns {*[]}
     */
    BurgersService.prototype.getBurgers = function () { // declare methods on the 'prototype' of the service
        return [{
            name: 'Luftsprung',
            description: 'Walnussbratling mit Gorgonzolacrème und Rauke',
            type: 'V',
            price: 7.8
        }, {
            name: 'Landei',
            description: 'gegrillte Hähnchenbrust, viele Sprossen und Schnittlauchsoße',
            type: 'B',
            price: 7.5
        }, {
            name: 'Klassik',
            description: 'Die Mutter aller Burger',
            type: 'C',
            price: 6.5
        }];
    };

})();

