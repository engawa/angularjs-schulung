/**
 * Created by gerd on 17.10.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.views.menu', [])
        .controller('MenuCtrl', MenuCtrl);

    /**
     *
     * @constructor
     */
    function MenuCtrl(BurgersFactory, BurgersService, BurgersValue) {
        // Public API
        this.getBurgerCount = getBurgerCount;

        // retrieve a list of burgers to be displayed in the view
        this.burgers = BurgersFactory.getBurgers(); // either use a factory
        //this.burgers = BurgersService.getBurgers(); // or a service, they are mutually the same
        //this.burgers = BurgersValue; // or a value

        /**
         *
         * @returns {Number}
         */
        function getBurgerCount() {
            return this.burgers.length;
        }
    }

})();

