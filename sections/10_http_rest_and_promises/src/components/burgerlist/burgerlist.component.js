/**
 * Created by gerd on 17.10.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.components.burgerlist', [])
        .directive('cbBurgerList', BurgerListComponent);

    function BurgerListComponent() {
        // Directive Definition Object (DDO)
        var directive = {
            restrict: 'E',
            templateUrl: 'src/components/burgerlist/burgerlist.html',
            controller: BurgerListCtrl,
            controllerAs: 'list'
        };
        return directive;
    }

    /**
     *
     * @constructor
     */
    function BurgerListCtrl(BurgersService) {
        var vm = this;

        // Public API
        this.getBurgerCount = getBurgerCount;

        // asynchronously load list of burgers
        BurgersService.getBurgers()
            .then(function (burgers) {
                return (vm.burgers = burgers);
            })
            .then(function (burgers) {
                // calculate length of longest description
                vm.maxchars = burgers.reduce(function (length, burger) {
                    return burger.description.length > length ? burger.description.length : length;
                }, 0);
            });

        /**
         *
         * @returns {Number}
         */
        function getBurgerCount() {
            return this.burgers && this.burgers.length;
        }
    }

})();