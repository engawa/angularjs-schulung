/**
 * Created by gerd on 18.10.15.
 */
angular
    .module('cleanburger.components', [
        'cleanburger.components.burgerlist',
        'cleanburger.components.burgerform',
        'cleanburger.components.burgerpreview'
    ]);
