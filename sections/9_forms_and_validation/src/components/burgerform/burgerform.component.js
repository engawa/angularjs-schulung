/**
 * Created by gerd on 22.10.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.components.burgerform', [])
        .directive('cbBurgerForm', BurgerFormDirective);

    /* @ngInject */
    function BurgerFormDirective() {
        var directive = {
            restrict: 'E',
            templateUrl: 'src/components/burgerform/burgerform.html',
            scope: {},
            controller: BurgerFormCtrl,
            controllerAs: 'vm',
            bindToController: {
                burger: '='
            }
        };
        return directive;
    }

    /* @ngInject */
    function BurgerFormCtrl(BurgersService) {

        this.types = BurgersService.getTypes();
    }

})();

