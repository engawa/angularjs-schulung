/**
 * Created by gerd on 16.10.15.
 */
(function () {
    'use strict';

    // 1. declare a new module by giving it a name
    // 2. declare - optional - dependencies upon other modules inside of the square brackets
    angular.module('cleanburger', []);

})();