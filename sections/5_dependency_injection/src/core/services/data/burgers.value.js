/**
 * Created by gerd on 17.10.15.
 */
angular
    .module('cleanburger.core.services.data')
    .value('BurgersValue', [{
        name: 'Luftsprung',
        description: 'Walnussbratling mit Gorgonzolacrème und Rauke',
        type: 'V',
        price: 7.8
    }, {
        name: 'Landei',
        description: 'gegrillte Hähnchenbrust, viele Sprossen und Schnittlauchsoße',
        type: 'B',
        price: 7.5
    }, {
        name: 'Klassik',
        description: 'Die Mutter aller Burger',
        type: 'C',
        price: 6.5
    }]);
