/**
 * Created by gerd on 17.10.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.core.services.data')
        .service('BurgersService', BurgersService);

    /**
     *
     * @constructor
     */
    function BurgersService(BurgersValue) { // services can be injected to build a tree of dependencies
        this.burgers = BurgersValue;
    }

    /**
     *
     * @returns {*[]}
     */
    BurgersService.prototype.getBurgers = function () {
        return this.burgers;
    };

})();

