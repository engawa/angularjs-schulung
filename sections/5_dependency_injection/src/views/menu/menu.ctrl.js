/**
 * Created by gerd on 17.10.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.views.menu', [])
        //.controller('MenuCtrl', ['BurgersService', MenuCtrl]) // EITHER explicitly declare service names (constructor function as last array entry)
        .controller('MenuCtrl', MenuCtrl);

    // OR explicitly declare service names by decorating the constructor function
    MenuCtrl.$inject = ['BurgersService'];

    /**
     *
     * @constructor
     * @param {BurgersService} BurgersService the service instance to be injected
     */
    // minification (e.g. UglifyJS) renames parameter names
    /* @ngInject */ // OR make use of the ngAnnotate tool in your build chain to create the ```$inject``` decorator
    function MenuCtrl(BurgersService) { // if NOT explicitly declared, parameter name(s) have to match the name(s) of the respective service(s)
        // Public API
        this.getBurgerCount = getBurgerCount;

        // retrieve a list of burgers to be displayed in the view
        this.burgers = BurgersService.getBurgers();

        /**
         *
         * @returns {Number}
         */
        function getBurgerCount() {
            return this.burgers.length;
        }
    }

})();

