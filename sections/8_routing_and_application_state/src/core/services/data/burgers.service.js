/**
 * Created by gerd on 17.10.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.core.services.data')
        .service('BurgersService', BurgersService);

    /**
     *
     * @constructor
     */
    function BurgersService(BurgersValue) {
        this.burgers = BurgersValue;
    }

    /**
     *
     * @returns {*[]}
     */
    BurgersService.prototype.getBurgers = function () {
        return this.burgers;
    };


    /**
     *
     * @param name
     * @returns {*}
     */
    BurgersService.prototype.getBurgerByName = function (name) {
        return this.burgers.filter(function (burger) {
            return burger.name === name;
        })[0];
    };

})();

