/**
 * Created by gerd on 17.10.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.components.burgerpreview', [])
        .directive('cbBurgerPreview', BurgerPreviewComponent);
    /**
     *
     */
    function BurgerPreviewComponent() {
        var directive = {
            restrict: 'E',
            templateUrl: 'src/components/burgerpreview/burgerpreview.html',
            scope: {}, // create an isolated scope for each component instance
            controller: BurgerPreviewCtrl,
            controllerAs: 'preview',
            bindToController: {
                burger: '=' // add two-way binding for the data to be displayed
            }
        };
        return directive;
    }

    /**
     *
     * @constructor
     */
    function BurgerPreviewCtrl() {
        // Public API
        this.isVegetarian = isVegetarian;

        /**
         *
         * @param {Object} burger
         * @returns {boolean} true, if the given burger is suited for vegetarians, otherwise false
         */
        function isVegetarian(burger) {
            return burger.type === 'V';
        }
    }

})();

