/**
 * Created by gerd on 25.10.15.
 */
(function () {
    'use strict';

    angular
        .module('cleanburger.views.details', [])
        .controller('DetailsCtrl', DetailsCtrl);

    DetailsCtrl.$inject = ['BurgersService', '$routeParams'];

    /* @ngInject */
    function DetailsCtrl(BurgersService, $routeParams) {

        this.burger = BurgersService.getBurgerByName($routeParams.name);
    }

})();

