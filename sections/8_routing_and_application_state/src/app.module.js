/**
 * Created by gerd on 16.10.15.
 */
(function () {
    'use strict';

    // 1. declare a new module by giving it a name
    // 2. declare - optional - dependencies upon other modules inside of the square brackets
    angular.module('cleanburger', [
        'ngRoute',

        'cleanburger.core.filters',
        'cleanburger.core.services',
        'cleanburger.components',
        'cleanburger.views.details'
    ])
        .config(function ($routeProvider) {
            "use strict";
            // declare rules to match a url to a specific component / controller
            $routeProvider
                .when('/burgers', {
                    template: '<cb-burger-list></cb-burger-list>'
                })
                .when('/burgers/:name', { // use : notation to declare variables
                    templateUrl: 'src/views/details/details.html',
                    controller: 'DetailsCtrl as details'
                })
                .otherwise('/burgers'); // declare default route when no rule matches
        });

})();