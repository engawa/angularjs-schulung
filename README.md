# README #

This README documents whatever steps are necessary to get this application up and running.

### What is this repository for? ###

* This repository is used for educational purposes during AngularJS workshops.
* Version 1.2

### How do I get set up? ###

* Make sure you have [Node.js](http://nodejs.org) installed.
* Run ```npm i -g bower wiredep``` to install global dependencies.
* Run ```npm start``` to install local dependencies and to start a local web server.
* Example code can be found in the ```sections``` folder.

### Who do I talk to? ###

* [Engawa](mailto:infos@engawa.de)