/**
 * Created by gerd on 18.10.15.
 */
var Hapi = require('hapi');
var opn = require('opn');
var api = require('./lib/api');
// Create a server with a host and port
var server = new Hapi.Server();
server.register(require('inert'), function (error) {
    "use strict";
    if (error) {
        throw error;
    }
    server.connection({
        host: '127.0.0.1',
        port: 8000
    });
    // Add the route
    server.route({
        method: 'GET',
        path: '/sections/{param*}',
        handler: {
            directory: {
                path: 'sections'
            }
        }
    });
    server.route({
        method: 'GET',
        path: '/app/{param*}',
        handler: {
            directory: {
                path: 'app'
            }
        }
    });
    // REST API
    server.route({
        method: 'GET',
        path: '/api/burgers',
        handler: function (req, reply) {
            api.getBurgers()
                .then(function (burgers) { return reply(burgers); });
        }
    });
    server.route({
        method: 'GET',
        path: '/api/burgers/{name}',
        handler: function (req, reply) {
            api.getBurgerByName(req.params['name'])
                .then(function (burger) { return reply(burger); });
        }
    });
    server.route({
        method: 'PUT',
        path: '/api/burgers/{name}',
        handler: function (req, reply) {
            api.updateBurger(req.params['name'], req.payload)
                .then(function (burger) { return reply(burger); });
        }
    });
    // Start the server
    server.start(function () {
        console.log('Server running at:', server.info.uri);
        opn(server.info.uri + "/sections");
    });
});
//# sourceMappingURL=index.js.map